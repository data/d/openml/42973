# OpenML dataset: ct-slice-localization

https://www.openml.org/d/42973

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: F. Graf, H.-P. Kriegel, M. Schubert, S. Poelsterl, A. Cavallaro 
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Relative+location+of+CT+slices+on+axial+axis) - 2011
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**Relative location of CT slices on axial axis Data Set**

The dataset consists of 384 features extracted from CT images. The class variable is numeric and denotes the relative location of the CT slice on the axial axis of the human body. The data was retrieved from a set of 53500 CT images from 74 different patients (43 male, 31 female).  Each CT slice is described by two histograms in polar space. The first histogram describes the location of bone structures in the image, the second the location of air inclusions inside of the body.  Both histograms are concatenated to form the final feature vector.  Bins that are outside of the image are marked with the value -0.25.  The class variable (relative location of an image on the axial axis) was constructed by manually annotating up to 10 different distinct landmarks in each CT Volume with known location. The location of slices in between 
landmarks was interpolated.

### Attribute information

1. patientId: Each ID identifies a different patient 
2. - 241.: Histogram describing bone structures 
242. - 385.: Histogram describing air inclusions 
386. reference: Relative location of the image on the axial axis (class 
value). Values are in the range [0; 180] where 0 denotes 
the top of the head and 180 the soles of the feet.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42973) of an [OpenML dataset](https://www.openml.org/d/42973). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42973/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42973/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42973/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

